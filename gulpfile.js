var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    del = require('del'),
    connect = require('gulp-connect')
    ;

var bower  = require('main-bower-files');
var ngAnnotate = require('gulp-ng-annotate');

gulp.task('vendors', function () {
    return gulp.src([
            // TODO Automate this!
            //Manually add dependent bower components here
            //'src/bower_components/jquery/jquery.min.js',
            'src/bower_components/angular/angular.min.js',
            'src/bower_components/angular-ui-router/release/angular-ui-router.min.js',
            'src/bower_components/ngstorage/ngStorage.min.js'
        ])
        .pipe(concat('vendors.min.js'))
        .pipe(gulp.dest('dist/app'));
});

gulp.task('scripts', function() {
     gulp.src(
            [
                // Make sure when we concat / combine files, we are putting the *.modules.js first
                'src/app/**/*.module.js',
                'src/app/**/*.controller.js',
                'src/app/**/*.service.js'
            ]
        )
        // Annotate before uglify so the code get's min'd properly.
        .pipe(ngAnnotate({
            add: true,
            single_quotes: true
        }))
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('default'))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/app'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('dist/app'))
        .pipe(notify({ message: 'Scripts task complete' }))
        .pipe(connect.reload())
        ;
});

gulp.task('htmltemplates', function() {
     gulp.src('src/app/**/*.html')
        .pipe(gulp.dest('dist/app'))
        .pipe(notify({ message: 'Html templates task complete' }))
        .pipe(connect.reload())
        ;
});

gulp.task('clean', function() {
    //return del(['dist/assets/css', 'dist/assets/js', 'dist/assets/img']);
    return del(['dist/app']);
});

gulp.task('default', ['clean'], function() {
    gulp.start('vendors', 'scripts', 'htmltemplates');
});
gulp.task('build', ['default']);

gulp.task('connectDev', function() {
    connect.server({
        port: 8080,
        root: 'src',
        livereload: true,
        fallback: 'src/index.html'
    });
});

gulp.task('connectBuild', function() {
    connect.server({
        port: 8080,
        root: './dist/',
        livereload: true,
        fallback: 'dist/index.html'
    });
});

gulp.task('watch', function () {
    //gulp.watch(', ['scripts']).pipe(connect.reload());
    gulp.watch(
        [
            './src/app/**/*.html',
            './src/app/**/*.js'
        ],
        ['htmltemplates', 'scripts']
    );
});



gulp.task('dev', ['connectDev', 'watch']);
gulp.task('live', ['vendors', 'connectBuild', 'watch']);

// Usage:
// To rebuild dist
//      $ gulp
//      $ gulp build

// To run dev
//      $ gulp dev
//      Open https://localhost:8080/index.html

// To run live test with minified files
//      $ gulp live
//      Open https://localhost:8080/index.html