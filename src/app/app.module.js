(function() {
    'use strict';

    /* @ngInject */
    angular.module('app', [
            /*
             * Order is not important. Angular makes a
             * pass to register all of the modules listed
             * and then when app.dashboard tries to use app.data,
             * its components are available.
             */

            /*
             * Everybody has access to these.
             * We could place these under every feature area,
             * but this is easier to maintain.
             */
            'app.core',

            /*
             * Feature areas
             */
            'app.home',
            'app.user',
            'app.login',
            'app.logout',
            'app.subreddit',
            'app.comments',

            /*
             * Global services
             */
            'app.coreService',
            'app.postsService',

            /**
             * Global directives
             */
            'app.navigation'
        ])
        /* @ngInject */
        .config(configOptions)
        /* @ngInject */
        .config(defaultRouter)
        //.config(localStorageConfigs)
    ;


    /* @ngInject */
    function configOptions($locationProvider, $httpProvider) {
        // Enable HTML5 routing mode instead of using hash-routing.
        $locationProvider.html5Mode(true);

        // Enable CORS.
        $httpProvider.defaults.useXDomain = true;

        // Pass token for every request when it is available
        $httpProvider.interceptors.push([
            '$location',
            '$injector',
            '$q',
            '$localStorage',
            function ($location, $injector, $q, $localStorage) {
                console.log('Inside interceptor.');
                //localStorageService.get('foo');
                return {
                    'request': function (config) {
                        //if(localStorageService.get('token') !== null) {
                        config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
                        if (typeof $localStorage.token !== 'undefined') {

                            if ($location.path() != '/logout') {
                                //config.headers['Authorization'] = 'bearer ' + localStorageService.get('token');
                                //config.headers['Authorization'] = 'bearer ' + $localStorage.token;
                                config.headers.Authorization = 'bearer ' + $localStorage.token;
                                console.log('Set > Authorization : bearer ' + $localStorage.token);
                            } else {
                                console.log('Location is /logout. No sending Authorization header.');
                            }

                        } else {
                            console.log('Token in localStorage is not set.');
                        }
                        return config;
                    }
                };
            }
        ]);
    }

    /* @ngInject */
    function defaultRouter($urlRouterProvider) {
        // Register the default route.
        $urlRouterProvider.otherwise('/');
    }


    /* @ngInject */
    //function localStorageConfigs(localStorageServiceProvider) {
    //    console.log('Localstorage options set...');
    //    localStorageServiceProvider
    //        //.setPrefix('redditangular')
    //        //.setStorageType('localStorage')
    //        .setNotify(true, true)
    //    ;
    //}
})();
