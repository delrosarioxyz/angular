(function () {
    'use strict';

    angular.module('app.userService', [])
        .factory('userService', userService);

    /* @ngInject */
    function userService ($http, $q, coreService) {

        var service = {
            getMe: getMe
        };
        return service;

        function getMe() {
            // Create the future.
            var deferred = $q.defer();
            // Call the API to get stuff.

            $http.get(
                'https://oauth.reddit.com/api/v1/me'
            ).success(success).error(error);
            // Return the promise.
            return deferred.promise;

            // This callback will be called asynchronously when the response is available
            function success(data/*, status, headers, config*/) {
                deferred.resolve(data);
            }

            // Called asynchronously if an error occurs or server returns response with an error status.
            function error(data, status/*, headers, config*/) {
                deferred.reject(status);
            }
        }
    }

})();
