(function() {
    'use strict';

    angular
        .module('app.user', [
            'app.userService'
        ])
        .config(route);

    /* @ngInject */
    function route($stateProvider){
        $stateProvider
            .state('user', {
                url: '/user',
                templateUrl: './app/user/user.html',
                controller: 'userController',
                controllerAs: 'userController'
            }
        )
        ;
    }
})();
