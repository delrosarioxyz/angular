(function() {
    'use strict';

    angular
        .module('app.home')
        .controller('userController', userController);

    /* @ngInject */
    function userController($localStorage, userService) {
        /*jshint validthis: true */
        var vm = this;
        vm.title = 'User Page';
        vm.name = '';

        activate();

        function activate() {
            vm.name = $localStorage.name;
        }
    }
})();
