(function() {
    'use strict';

    angular
        .module('app.login')
        .controller('loginController', loginController);

    /* @ngInject */
    function loginController($location, $localStorage, $sessionStorage, loginService, coreService, userService) {
        /*jshint validthis: true */
        var vm = this;
        vm.title = 'Login';

        vm.state = null;
        vm.code = null;
        vm.token = null;
        vm.oAuthLink = coreService.getOAuth2Link();

        // Functions
        vm.getToken = getToken;
        vm.getMe = getMe;

        activate();
        function activate() {
            vm.state = $location.search().state;
            vm.code = $location.search().code ? $location.search().code : null;
            //var token = localStorageService.get('token');
            var token = $localStorage.token;
            vm.token = token;
            if(!token && vm.code) {
                getToken();
            } else {
                console.log('Not getting token for reddit.');
            }
        }

        /**
         * Get token from reddit
         * See https://github.com/reddit/reddit/wiki/OAuth2#retrieving-the-access-token
         */
         function getToken () {
            console.log('loginController.getToken()');
            console.log('vm.code: ' + vm.code);

            loginService.getToken(vm.code).then(success, failure);

            function success(data) {
                vm.token = data.access_token;
                $localStorage.token = vm.token;
                getMe();
            }

            // Failure.
            function failure(/* status */) {
            }
        }

        function getMe() {
            //console.log('Token successfully fetched. Now getting "/api/v1/me"');

            userService.getMe().then(success, failure);

            // Success.
            function success(data) {
                $localStorage.name = data.name;
                $localStorage.link_karma = data.link_karma;
                $localStorage.comment_karma = data.comment_karma;
                $localStorage.is_gold = data.is_gold;
                $localStorage.inbox_count = data.inbox_count;
            }

            // Failure.
            function failure(/* status */) {
                alert('Error! Unable to fetch user.');
            }
        }


    }
})();
