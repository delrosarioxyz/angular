(function() {
    'use strict';

    angular
        .module('app.login', [
            'app.loginService'
        ])
        .config(route);

    /* @ngInject */
    function route($stateProvider){
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: './app/login/login.html',
                controller: 'loginController',
                controllerAs: 'loginController'
            }
        )
        ;
    }
})();
