(function () {
    'use strict';

    angular.module('app.coreService', [])
        .factory('coreService', coreService);

    /* @ngInject */
    function coreService() {

        var service = {
            getClientId: getClientId,
            getClientSecret: getClientSecret,
            getAccessTokenUrl: getAccessTokenUrl,
            getRevokeTokenUrl: getRevokeTokenUrl,
            getGrantType: getGrantType,
            getRedirectUri: getRedirectUri,
            getDuration: getDuration,
            getScope: getScope,
            getOAuth2ClientIdAndSecret: getOAuth2ClientIdAndSecret,
            getOAuth2Link: getOAuth2Link
        };

        return service;

        function getClientId() {
            return '1g3g00NUqoL8AQ';
        }

        function getClientSecret() {
            return '';
        }

        function getAccessTokenUrl() {
            return 'https://www.reddit.com/api/v1/access_token';
        }

        function getRevokeTokenUrl() {
            return 'https://www.reddit.com/api/v1/revoke_token';
        }

        function getGrantType() {
            return 'authorization_code';
        }

        function getRedirectUri() {
            return 'http://localhost:8080/login';
        }

        function getDuration() {
            return 'permanent';
        }

        function getScope() {
            return 'identity,edit,flair,history,modconfig,' +
                'modflair,modlog,modposts,modwiki,mysubreddits,' +
                'privatemessages,read,report,save,submit,subscribe,vote,wikiedit,wikiread';
        }

        /**
         * String is client_id:client_secret
         * @returns {*}
         */
        function getOAuth2ClientIdAndSecret() {
            return btoa(service.getClientId() + ':' + service.getClientSecret());
        }

        /**
         * Link that user can click so that user is redirected to reddit to accept permissions
         *
         * @returns {string}
         */
        function getOAuth2Link() {
            return 'https://www.reddit.com/api/v1/authorize?' +
                'client_id=' + service.getClientId() +
                '&grant_type=' + service.getGrantType() +
                '&response_type=code' +
                '&state=' + getRandomStateNumber() +
                '&redirect_uri=' + service.getRedirectUri() +
                '&duration=' + service.getDuration() +
                '&scope=' + service.getScope();
        }

        /**
         *
         * @returns {number}
         */
        function getRandomStateNumber() {
            var max = 99999;
            var min = 2;
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

    }

})();
