"use strict";

describe("postService Test", function () {
    var postsService, httpBackend, localStorage;

    beforeEach(module("app"));

    beforeEach(inject(function(_postsService_, $httpBackend, $localStorage) {
        postsService = _postsService_;
        httpBackend = $httpBackend;
        localStorage = $localStorage;
    }));

    it("Unlogged user in should fetch frontpage posts", function () {
        localStorage.$reset();
        httpBackend.whenGET("https://www.reddit.com/.json?count=undefined").respond(
            {
                data: {
                    children: [
                        {
                            kind: "t3",
                            data: {
                                title: "Foobar"
                            }
                        },
                        {
                            kind: "t3",
                            data: {
                                title: "Reddit"
                            }
                        },
                        {
                            kind: "t3",
                            data: {
                                title: "Lorem Ipsum"
                            }
                        }
                    ]
                }
            }

        );

        postsService.getPosts().then(function(posts) {
            expect(posts.data.children[0].data.title).toEqual("Foobar");
            expect(posts.data.children[1].data.title).toEqual("Reddit");
            expect(posts.data.children[2].data.title).toEqual("Lorem Ipsum");
        });
        httpBackend.flush();
    });

    it("URL is oauth.reddit.com when loggedin", function() {
        localStorage.token = 'random_token_string';
        console.log(localStorage.token);
        httpBackend.whenGET("https://oauth.reddit.com?count=undefined").respond(
            {
                data: {
                    children: [
                        {
                            kind: "t3",
                            data: {
                                title: "Foobar"
                            }
                        },
                        {
                            kind: "t3",
                            data: {
                                title: "Reddit"
                            }
                        },
                        {
                            kind: "t3",
                            data: {
                                title: "Lorem Ipsum"
                            }
                        }
                    ]
                }
            }

        );

        postsService.getPosts().then(function(posts) {
            expect(posts.data.children[0].data.title).toEqual("Foobar");
            expect(posts.data.children[1].data.title).toEqual("Reddit");
            expect(posts.data.children[2].data.title).toEqual("Lorem Ipsum");
        });
        httpBackend.flush();
    });
});
