"use strict";

describe("coreService Test", function () {
    var coreService;

    beforeEach(module("app"));


    beforeEach(inject(function (_coreService_) {
        coreService = _coreService_;
    }));

    it("Params are set for OAuth2", function() {
        expect(coreService.getClientId()).toEqual('1g3g00NUqoL8AQ');
        expect(coreService.getClientSecret()).toEqual('');
        expect(coreService.getAccessTokenUrl()).toEqual('https://www.reddit.com/api/v1/access_token');
        expect(coreService.getRevokeTokenUrl()).toEqual('https://www.reddit.com/api/v1/revoke_token');
        expect(coreService.getGrantType()).toEqual('authorization_code');
        expect(coreService.getRedirectUri()).toEqual('http://localhost:8080/login');
        expect(coreService.getDuration()).toEqual('permanent');

        expect(coreService.getOAuth2ClientIdAndSecret()).toEqual(
            btoa('1g3g00NUqoL8AQ' + ':' + '')
        );

        expect(coreService.getScope()).toEqual(
            'identity,edit,flair,history,modconfig,' +
            'modflair,modlog,modposts,modwiki,mysubreddits,' +
            'privatemessages,read,report,save,submit,subscribe,vote,wikiedit,wikiread'
        );
    });
});
