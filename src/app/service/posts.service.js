(function () {
    'use strict';

    angular.module('app.postsService', [])
        .factory('postsService', postsService);

    /* @ngInject */
    function postsService($http, $q, $localStorage) {

        return  {
            setUrlForQuerying: getUrlForQuerying,
            getPosts: getPosts
        };

        function getUrlForQuerying(action, count, before, after, subreddit, commentKey, isLoggedIn) {
            var url = 'https://oauth.reddit.com';

            if (isLoggedIn === false) {
                url = 'https://www.reddit.com';
            }

            if (subreddit) {
                url = url + '/r/' + subreddit;
            }

            if (commentKey) {
                url = url + '/comments/' + commentKey;
            }

            if (isLoggedIn === false) {
                url = url + '/.json';
            }

            url = url + '?count=' + count;


            if (action == 'after') {
                url = url + '&' + action + '=' + after;
            }

            if (action == 'before') {
                url = url + '&' + action + '=' + before;
            }

            return url;
        }

        function getPosts(action, count, before, after, subreddit, commentKey) {
            var url;
            var isLoggedIn = false;

            if (typeof $localStorage.token !== 'undefined') {
                isLoggedIn = true;
            }

            url = getUrlForQuerying(action, count, before, after, subreddit, commentKey, isLoggedIn);

            var deferred = $q.defer();
            // Call the API to get stuff.
            $http.get(
                url
            ).success(success).error(error);
            // Return the promise.
            return deferred.promise;

            // This callback will be called asynchronously when the response is available
            function success(data/*, status, headers, config*/) {
                deferred.resolve(data);
            }

            // Called asynchronously if an error occurs or server returns response with an error status.
            function error(data, status/*, headers, config*/) {
                deferred.reject(status);
            }
        }
    }

})();
