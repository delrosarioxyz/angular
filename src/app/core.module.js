(function() {
    'use strict';

    angular.module('app.core', [
        //ngCookies,
        //ngMessages,
        //ngResource,
        //ngSanitize,
        'ui.router',
        'ngStorage'
    ])
    ;
})();
