(function() {
    'use strict';

    angular
        .module('app.subreddit', [
            //'app.userService'
        ])
        .config(route);

    /* @ngInject */
    function route($stateProvider){
        $stateProvider
            .state('subreddit', {
                    url: '/r/:subreddit',
                    templateUrl: './app/subreddit/subreddit.html',
                    controller: 'subredditController',
                    controllerAs: 'subredditController'
                }
            )
        ;
    }
})();
