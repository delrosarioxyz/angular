(function() {
    'use strict';

    angular
        .module('app.subreddit')
        .controller('subredditController', subredditController);

    /* @ngInject */
    function subredditController($stateParams, $localStorage, postsService) {
        /*jshint validthis: true */
        var vm = this;
        vm.title = 'Subreddit';

        vm.subreddit = $stateParams.subreddit;

        var items = 25;

        vm.title = 'Homepage';

        vm.storage = $localStorage;
        vm.posts = {};

        vm.before = null;
        vm.after = null;

        vm.count = items;
        vm.getPosts = getPosts;

        activate();
        function activate() {
            vm.getPosts();
        }

        function getPosts(actionClicked) {

            //console.log('Clicked on: ' + actionClicked);

            if (actionClicked == 'after') {
                vm.count += items;
            }

            if (actionClicked == 'before') {
                vm.count -= items;
            }

            postsService.getPosts(actionClicked, vm.count, vm.before, vm.after, vm.subreddit).then(success, failure);

            function success(data) {
                vm.after = data.data.after;
                vm.before = data.data.before;

                vm.posts = data.data.children;
            }

            // Failure.
            function failure(status) {
                console.log(status);
                if (status == 401) {
                    alert('Failed to get reddit.com frontpage. Need to implement token refresh.');
                } else {
                    alert('You need to be logged in...');
                }

            }
        }
    }
})();
