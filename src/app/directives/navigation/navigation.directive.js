(function () {
    'use strict';

    angular.module('app.navigation')
        .directive('navigationDirective', navigationDirective);

    function navigationDirective() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/directives/navigation/navigation.html',
            controller: 'navigationController',
            controllerAs: 'navigationController',
            bindToController: true
        };

        return directive;
    }

})();
