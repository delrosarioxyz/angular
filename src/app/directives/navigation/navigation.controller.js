(function() {
    'use strict';

    angular
        .module('app.navigation', [])
        .controller('navigationController', navigationController);

    /* @ngInject */
    function navigationController($localStorage, $sessionStorage, coreService) {
        /*jshint validthis: true */

        var vm = this;
        vm.storage = $localStorage;
        vm.oAuthLink = coreService.getOAuth2Link();
    }
})();
