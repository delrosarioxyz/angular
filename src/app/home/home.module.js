(function() {
    'use strict';

    angular
        .module('app.home', [
            'app.homeService'
        ])
        .config(route);

    /* @ngInject */
    function route($stateProvider){
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: './app/home/home.html',
                controller: 'homeController',
                controllerAs: 'homeController'
            },
            {reload: true}
        )
        ;
    }
})();
