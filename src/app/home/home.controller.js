(function() {
    'use strict';

    angular
        .module('app.home')
        .controller('homeController', homeController);

    /* @ngInject */
    function homeController($localStorage, postsService) {
        /*jshint validthis: true */
        var vm = this;

        var items = 25;

        vm.title = 'Homepage';

        vm.storage = $localStorage;
        vm.posts = {};

        vm.before = null;
        vm.after = null;

        vm.count = items;

        vm.getFrontpage = getFrontpage;

        activate();
        function activate() {
            vm.getFrontpage();
        }

        function getFrontpage(actionClicked) {

            console.log('Clicked on: ' + actionClicked);

            if (actionClicked == 'after') {
                vm.count += items;
            }

            if (actionClicked == 'before') {
                vm.count -= items;
            }

            postsService.getPosts(actionClicked, vm.count, vm.before, vm.after).then(success, failure);

            function success(data) {
                vm.after = data.data.after;
                vm.before = data.data.before;

                vm.posts = data.data.children;
            }

            // Failure.
            function failure(status) {
                console.log(status);
                if (status == 401) {
                    alert('Failed to get reddit.com frontpage. Need to implement token refresh.');
                } else {
                    alert('You need to be logged in...');
                }

            }
        }
    }
})();
