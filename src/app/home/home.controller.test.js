describe("homeController Test", function() {
    var $scope;
    var controller, httpBackend;

    beforeEach(function() {
        module("app");
        inject(function($rootScope, $controller, $httpBackend) {
            $scope = $rootScope.$new();
            controller = $controller("homeController", {
                $scope: $scope
            });

            httpBackend = $httpBackend;
        });
    });

    it("Should say Homepage", function() {
        expect(controller.title).toBe("Homepage");
    });

    it("Default count is 25", function() {
        expect(controller.count).toBe(25);
    });

    it("Unlogged user in should fetch frontpage posts", function () {
        controller.after = 'xxx';
        controller.getFrontpage('after');
        console.log(controller.after);
    });
});
