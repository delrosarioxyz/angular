(function() {
    'use strict';

    angular
        .module('app.comments')
        .controller('commentsController', commentsController);

    /* @ngInject */
    function commentsController($stateParams, $localStorage, postsService) {
        /*jshint validthis: true */
        var vm = this;
        vm.title = 'Comments';

        vm.subreddit = $stateParams.subreddit;
        vm.key = $stateParams.key;
        vm.slug = $stateParams.slug;

        var items = 25;

        vm.title = 'Comments';

        vm.storage = $localStorage;
        vm.post = {};
        vm.comments = {};

        vm.before = null;
        vm.after = null;

        vm.count = items;
        vm.getComments = getComments;

        activate();
        function activate() {
            vm.getComments();
        }

        function getComments(actionClicked) {

            console.log('Clicked on: ' + actionClicked);

            if (actionClicked == 'after') {
                vm.count += items;
            }

            if (actionClicked == 'before') {
                vm.count -= items;
            }

            postsService.getPosts(actionClicked, vm.count, vm.before, vm.after, vm.subreddit, vm.key).then(success, failure);

            function success(data) {

                vm.after = data[1].data.after;
                vm.before = data[1].data.before;

                vm.post = data[0].data.children[0].data;

                vm.comments = data[1].data.children;
            }

            // Failure.
            function failure(status) {
                console.log(status);
                if (status == 401) {
                    alert('Failed to get reddit.com frontpage. Need to implement token refresh.');
                } else {
                    alert('You need to be logged in...');
                }

            }
        }
    }
})();
