(function() {
    'use strict';

    angular
        .module('app.comments', [])
        .config(route);

    /* @ngInject */
    function route($stateProvider){
        $stateProvider
            .state('comments', {
                    url: '/r/:subreddit/comments/:key/:slug/',
                    templateUrl: './app/comments/comments.html',
                    controller: 'commentsController',
                    controllerAs: 'commentsController'
                }
            )
        ;
    }
})();
