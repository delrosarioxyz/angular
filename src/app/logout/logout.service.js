(function () {
    'use strict';

    angular.module('app.logoutService', [])
        .factory('logoutService', logoutService);

    /* @ngInject */
    function logoutService ($http, $q, coreService, $localStorage) {
        /*jshint validthis: true */

        var service = {
            revokeToken: revokeToken
        };
        return service;

        function revokeToken() {
            console.log('logoutService.revokeToken()');

            // Set headers for POST request
            $http.defaults.headers.common.Authorization = 'Basic ' + coreService.getOAuth2ClientIdAndSecret();
            $http.defaults.headers.post['Content-Type'] =  'application/x-www-form-urlencoded; charset=UTF-8' ;

            var deferred = $q.defer();
            // Call the API to get stuff.
            $http.post(
                coreService.getRevokeTokenUrl(),
                'token=' + $localStorage.token
            ).success(success).error(error);
            // Return the promise.
            return deferred.promise;

            // This callback will be called asynchronously when the response is available
            function success(data/*, status, headers, config*/) {
                deferred.resolve(data);
            }

            // Called asynchronously if an error occurs or server returns response with an error status.
            function error(data, status/*, headers, config*/) {
                deferred.reject(status);
            }
        }
    }

})();
