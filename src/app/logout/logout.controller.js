(function() {
    'use strict';

    angular
        .module('app.logout')
        .controller('logoutController', logoutController);

    /* @ngInject */
    function logoutController($scope, $location, $localStorage, $sessionStorage, logoutService, coreService) {
        /*jshint validthis: true */

        var vm = this;

        // Functions
        vm.revokeToken = revokeToken;

        activate();
        function activate() {
            revokeToken();
        }

        function revokeToken() {
            console.log('logoutController.revokeToken()');

            logoutService.revokeToken(vm.code).then(success, failure);

            function success(data) {
                console.log('Revoke successful. Clearing localStorage.');
                $localStorage.$reset();
            }

            // Failure.
            function failure(/* status */) {
            }
        }

    }
})();
