(function() {
    'use strict';

    angular
        .module('app.logout', [
            'app.logoutService'
        ])
        .config(route);

    /* @ngInject */
    function route($stateProvider){
        $stateProvider
            .state('logout', {
                    url: '/logout',
                    templateUrl: './app/logout/logout.html',
                    controller: 'logoutController',
                    controllerAs: 'logoutControllerr'
                }
            )
        ;
    }
})();
