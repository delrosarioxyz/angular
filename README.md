# README #

# First steps

Install `npm` **without** `sudo`

I have forgotten which link I used but this one seems reliable:

* http://www.johnpapa.net/how-to-use-npm-global-without-sudo-on-osx/
* https://github.com/sindresorhus/guides/blob/master/npm-global-without-sudo.md
* https://gist.github.com/DanHerbert/9520689

After installing `npm` install `bower`.

Then run `npm install`.
Then run `bower install`.

#Usage:

## To run dev
    $ gulp dev
    Open https://localhost:8080/index.html

## To run Karma for unit testing while developing
Best to do it in Webstorm inside the terminal palette ;)

    $ npm test

## To run live test with minified files
    $ gulp live
    Open https://localhost:8080/index.html

## To build dist
    $ gulp
    $ gulp build

